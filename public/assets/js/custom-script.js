Split(['#split-0', '#split-1', '#split-2'],{
    snapOffset: 10,
    gutterSize: 5,
    sizes: [15, 65, 20]
})
Split(['#split-3', '#split-4'], {
    direction: 'vertical',
    minSize: 150,
    gutterSize: 5,
    snapOffset: 10,
})