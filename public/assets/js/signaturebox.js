var boxArray = [];
$("#addSignature").click(function(){
    var boxObject = {
        name : "demo",
        signatureImage : "",
        designation : "demo designation",
        positionX : "0",
        positionY : "0",
        height: "",
        width: ""
    }
    boxArray.push(boxObject);
    $(".single-signature-main-wrapper").remove();
    boxArray.map((item, index) => {
        $(".signature-wrapper").append('<div class="single-signature-main-wrapper single-signature-'+index+'" dataindex="'+index+'" style="width: '+item.width+'; height: '+item.height+'; transform: translate('+item.positionX+'px, '+item.positionY+'px);"><div dataindex="'+index+'" class="single-signature-wrapper"><span class="name">'+item.name+'</span><br /><span class="designation">'+item.designation+'</span></div><span class="clossButton">X</span></div>')
    });
    console.log('boxArray', boxArray);
});



interact('.single-signature-main-wrapper')
.draggable({
    // enable inertial throwing
    inertia: true,
    // keep the element within the area of it's parent
    modifiers: [
      interact.modifiers.restrictRect({
        restriction: 'parent',
        endOnly: false
      })
    ],
    // enable autoScroll
    autoScroll: true,

    listeners: {
      // call this function on every dragmove event
      move: dragMoveListener,

      // call this function on every dragend event
      end (event) {
        var textEl = event.target.querySelector('p')

        textEl && (textEl.textContent =
          'moved a distance of ' +
          (Math.sqrt(Math.pow(event.pageX - event.x0, 2) +
                     Math.pow(event.pageY - event.y0, 2) | 0))
            .toFixed(2) + 'px')
      }
    }
  })
  .resizable({
    // resize from all edges and corners
    edges: { left: true, right: true, bottom: true, top: true },

    listeners: {
      move (event) {
        var target = event.target
        var x = (parseFloat(target.getAttribute('data-x')) || 0)
        var y = (parseFloat(target.getAttribute('data-y')) || 0)

        // update the element's style
        target.style.width = event.rect.width + 'px'
        target.style.height = event.rect.height + 'px'

        // translate when resizing from top or left edges
        x += event.deltaRect.left
        y += event.deltaRect.top

        target.style.transform = 'translate(' + x + 'px,' + y + 'px)'

        target.setAttribute('data-x', x)
        target.setAttribute('data-y', y)
        var index = $(event.target).attr('dataindex');
        boxArray[index].width = target.style.width;
        boxArray[index].height = target.style.height;
      }
    },
    modifiers: [
      // keep the edges inside the parent
      interact.modifiers.restrictEdges({
        outer: 'parent'
      }),

      // minimum size
      interact.modifiers.restrictSize({
        min: { width: 100, height: 50 }
      })
    ],

    inertia: false
  })

function dragMoveListener (event) {

  var target = event.target
  // keep the dragged position in the data-x/data-y attributes
  var x = (parseFloat(target.getAttribute('data-x')) || 0) + event.dx
  var y = (parseFloat(target.getAttribute('data-y')) || 0) + event.dy

  // translate the element
  target.style.transform = 'translate(' + x + 'px, ' + y + 'px)'

  // update the posiion attributes
  target.setAttribute('data-x', x)
  target.setAttribute('data-y', y)
  var index = $(event.target).attr('dataindex');
  boxArray[index].positionX = x;
  boxArray[index].positionY = y;
}

// this function is used later in the resizing and gesture demos
window.dragMoveListener = dragMoveListener

var currentIndex = "";

////
$(document).on('click','.single-signature-wrapper', function() {
    
    var index = $(this).attr('dataindex');
    currentIndex = index;
    $(".input-from-wrapper").removeClass('d-none');
});

$("#employeeDataSubmitClose").click(function() {
    $(".input-from-wrapper").addClass('d-none');
}); 

$(document).on('click','.clossButton', function() {
  $(".input-from-wrapper").addClass('d-none');
    var index = $(this).parent('.single-signature-main-wrapper').attr('dataindex');
    boxArray.splice(index, 1);
    $('.single-signature-'+index).remove();
    $(".input-from-wrapper").addClass('d-none');
    setTimeout(function() {
      
    }, 1000)
});
$(document).on("keyup", "#officerName", function(){
    boxArray[currentIndex].name = $(this).val();
    boxArray.map((item, index) => {
        $(".signature-wrapper").append('<div class="single-signature-main-wrapper single-signature-'+index+'" dataindex="'+index+'" style="width: '+item.width+'; height: '+item.height+'; transform: translate('+item.positionX+'px, '+item.positionY+'px);"><div dataindex="'+index+'" class="single-signature-wrapper"><span class="name">'+item.name+'</span><br /><span class="designation">'+item.designation+'</span></div><span class="clossButton">X</span></div>')
    });
});
$(document).on("keyup", "#officerDesignation", function(){
    boxArray[currentIndex].designation = $(this).val();
    boxArray.map((item, index) => {
        $(".signature-wrapper").append('<div class="single-signature-main-wrapper single-signature-'+index+'" dataindex="'+index+'" style="width: '+item.width+'; height: '+item.height+'; transform: translate('+item.positionX+'px, '+item.positionY+'px);"><div dataindex="'+index+'" class="single-signature-wrapper"><span class="name">'+item.name+'</span><br /><span class="designation">'+item.designation+'</span></div><span class="clossButton">X</span></div>')
    });
});

