<div class="footer bg-white d-flex flex-lg-column shadow-sm" id="kt_footer">
    <!--begin::Container-->
    <div class="container-fluid d-flex flex-column flex-md-row align-items-center justify-content-between py-3">
        <!--begin::Copyright-->
        <!--begin::Nav-->
        <div class="d-flex align-items-center">
            <span>Partners: </span>
            <div class="partners d-flex">
                <img class="pl-2" src="{{ asset('assets/images/a2i-Logo-set.png') }}">
                <!-- <a href="#" title="Bangladesh">
                    <img class="pl-2" src="/nothi-next/img/partners/ban-gov_logo.png" alt="bangladesh"></a>
                <a href="#" title="a2i">
                    <img class="pl-2" src="/nothi-next/img/partners/a_a2i-logo.jpg" alt="a2i"></a>
                <a href="#" title="BCC">
                    <img class="pl-2" src="/nothi-next/img/partners/bcc_logo.png" alt="BCC"></a>
                <a href="#" title="DOICT">
                    <img class="pl-2" src="/nothi-next/img/partners/doict_logo.jpg" alt="DOICT"></a>
                <a href="#" title="TAPPWARE">
                    <img class="pl-2" src="/nothi-next/img/partners/tappware_logo.png" alt="tappware"></a> -->
            </div>
        </div>
        <!--end::Nav-->
        <div class="text-dark">
            <span class="text-dark font-weight-bold">© Copyright 2021, </span>
            <a href="https://a2i.gov.bd/" target="_blank" class="text-dark text-hover-primary font-weight-bold">a2i Program </a>
        </div>
        <!--end::Copyright-->
        <!--end::Nav-->
        <div class="text-dark">
            <span class="text-dark font-weight-bold">Develop &amp; Maintanance By, </span>
            <a href="https://tappware.com/" target="_blank" class="text-dark text-hover-primary">Tappware </a>
        </div>
        <!--end::Copyright-->
        
    </div>
    <!--end::Container-->
</div>