@extends('fullWidthLayout')
@section('content')
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.2.228/pdf.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/interactjs@1.10.11/dist/interact.min.js"></script>
<div class="split">
    <div id="split-0">
        <div class="p-5">
            <h4>Pages</h4>
            <div id="checkboxList" class="mt-3">
            </div>
        </div>
    </div>
    <div id="split-1" style="width: auto !important;">
        <div class="row m-0 page-title-wrapper d-md-flex align-items-md-center">
            <div class="col-md-6">
                <div class="title py-2">
                    <button class="btn btn-success btn-square" id="fileUpload" type="button"><i class="fas fa-upload"></i> Load PDF</button>
                    <button class="btn btn-primary btn-square" id="addSignature" type="button"><i class="fas fa-plus"></i> Add New Signature</button>
                </div>
            </div>
        </div>
        <div class="p-5">
            <div class="d-flex align-item-center justify-content-between mt-2 mb-2">
                <div>
                    <div id="pdf-meta">
                        <div id="page-count-container">Page <div id="pdf-current-page"></div> of <div id="pdf-total-pages"></div></div>
                    </div>
                </div>
                <div>
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" id="pdf-prev" class="btn btn-primary btn-square">Previous</button>
                        <button type="button" id="pdf-next" class="btn btn-primary btn-square">Next</button>
                    </div>
                </div>
            </div>
            <div class="text-center">
                <div id="pdf-main-container" >
                    <div id="pdf-loader">Loading document ...</div>
                    <div id="pdf-contents">
                        <div class="position-relative pdf-viewer-wrapper" id="pdf-viewer-wrapper">
                            <canvas id="pdf-canvas" width="757"></canvas>
                            <div class="signature-wrapper">
                                <!-- <div class="position-relative d-inline-block single-signature-main-wrapper"><div class="single-signature-wrapper" dataindex="0" style="transform: translate(0px, 0px);"><span class="name">demo--1</span><br><span class="designation">demo designation</span></div><span class="clossButton">X</span></div> -->
                            </div>
                        </div>
                        
                        <div id="page-loader">Loading page ...</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="split-2">
        <div class="p-5">
            <div class="input-from-wrapper mt-5 d-none">
                <div class="form-group mb-3">
                    <label for="officerName">Signature Image</label>
                    <input type="file" class="form-control"/>
                </div>
                <div class="form-group mb-3">
                    <label for="officerName">Name</label>
                    <input type="text" class="form-control" name="officer_name" id="officerName" placeholder="Officer Name">
                </div>
                <div class="form-group mb-3">
                    <label for="officerDesignation">Designation</label>
                    <input type="text" class="form-control" name="officer_designation" id="officerDesignation" placeholder="Officer Designation">
                </div>
                <hr />
                {{-- <div class="float-end mt-3">
                    <button type="button" class="btn btn-primary btn-square" id="employeeDataSubmit">Submit</button>
                    <button type="button" class="btn btn-danger btn-square ml-2" id="employeeDataSubmitClose">Cancel</button>
                </div> --}}
                <div class="radio">
                    <label><input type="radio" name="optradio" checked> All Pages</label>
                  </div>
                  <div class="radio">
                    <label><input type="radio" name="optradio"> Pages</label>
                  </div>
                  <div class="form-group">
                    <input type="text" class="form-control" placeholder="1,2,3" />
                  </div>
            </div>
        </div>
    </div>
</div>
@endsection